#!/usr/bin/env /bin/bash

# Forked from https://gitlab.com/ayufan-repos/tests/step-runner-tests/-/blob/main/steps/shell/bash.yml

set -xeo pipefail

# move to job directory
cd "$JOB_DIR"

# execute job script
eval "$JOB_SCRIPT"
